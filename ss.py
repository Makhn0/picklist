import requests
from requests.auth import HTTPBasicAuth
import os.path as path
import datetime
import pickle as pk
from pp import *
from secrets import key, secret

def dateString():
    date_format="%m-%d-%Y"

    now=datetime.datetime.now()
    date_str=now.strftime(date_format)
    return date_str
#r=requests.get("https://www.github.com/Makhn0",auth=HTTPBasicAuth("Makhn0",""))

#jjr2=requests.post('http://httpbin.org/post', data={'key':'value'},params={'foo':"bar"})




def get_orders():
    """Retrieves the array of orders dictionaries from the shipstation api"""
    orders=[]
    page=1
    r=requests.get("https://ssapi.shipstation.com/orders",
                 auth=HTTPBasicAuth(key,secret),
            params={"orderStatus":"awaiting_shipment",
                    "page":str(page),
                    "pageSize":"500"
                   }
    )

    json=r.json()#micro-optimization 
    pages=json['pages']
    orders.extend(json['orders'])
    
    # print(pages)
    # pprint(r.json()['orders'])
    for i in range(1,pages):

        r=requests.get("https://ssapi.shipstation.com/orders",
                 auth=HTTPBasicAuth(key,secret),
            params={"orderStatus":"awaiting_shipment",
                    "page":str(i),
                    "pageSize":"500"
                   }
        )
        orders.extend(r.json()['orders'])
    # pprint(orders)
    return orders

#j=json.loads(str(r.json()))

def makeListData(orders):
    """takes the appropriate fields from the order objects in r.json()['orders']
        and puts them in 2D array"""
    ItemsByOrder=[ a['items'] for a in orders ]
    Items=[]
    for x in ItemsByOrder:
        Items.extend(x)

    #PickListData=[ (i['quantity'] ,i['name'] , i['sku']) for i in Items]
    RawPickListData=[ [i['quantity'] ,i['name'].strip()+" "+i['sku'] ] for i in Items]
    RawPickListDataSku=[ [i['quantity'] ,i['name'],i['sku'] ] for i in Items]
    return RawPickListData


def requestTest():
    """just used for testing purposes """
    r=requests.get("https://ssapi.shipstation.com/orders",
                   auth=HTTPBasicAuth(key,secret),
                   params={#"orderStatus":"_shipment"
                            "createDateStart":"2018-7-17",
                       "page":str(2),
                    "pageSize":"500"

                       
                   }
    )
    json=r.json()
    if 'Message 'in r.json().keys():
        print(json()['Message'])
    pprint(json.keys())

    with open('./lastOrders','w+') as f:
        f.write(
            # json.__str__()
            pformat(json,
                         indent=1)
        )
    with open('rtext','w+') as f:
        f.write(r.text)
    # pprint(json)
    n=len(json['orders'])
    if n<20:
        pass
        # pprint(json())
    return r

# r=requestTest()

def clearSaved():
    pk.dump(["",[]],open("savedOrder","wb"))

def getSavedOrders(filename='savedOrder'):
    today=dateString()
    try:
        return pk.load(open(filename,"rb"))
    except EOFError:
        return "",[]
def setSavedOrder(filename='savedOrder',date="",orders=[]):
        pk.dump([date,orders],open(filename,"wb"))

def getTodaysOrders(force=False):
    """gets orders from shipstation if haven't accessed today,
        otherwise gets the saved orders from 'savedOrder'"""


    savedDate,savedOrders=getSavedOrders()
    today=dateString()

    if savedDate==today:
        print("found saved data")
        return makeListData(savedOrders)
    else:
        orders=get_orders()
        setSavedOrder(date=today,orders=orders)
        return makeListData(orders)


def getAllOrders():
    return makeListData(get_orders())
def makeMissing():
    """called to get the orders that were on the morning picklist
        but which shipping has not received yet"""

    newOrders=get_orders()

    date, savedOrders=getSavedOrders()

    missingOrders=[]
    for order in newOrders:
        if order in savedOrders:
            pprint(order['items'][0]['name'])
            missingOrders.append(order)

    #@missingOrders=[order for order in newOrders if order in firstOrders]
    return makeListData(missingOrders)
if __name__=="__main__":
    print(len(makeMissing()))

    #pprint("RawPIckListData");pprint(RawPickListData)
    # pprint(Items)
    # pprint(PickListData)
    #export_items()
