#!/bin/python3
"""front end of picklist making software"""
#import sendOut.py
import shipcsv
import dept
import toXlsx
import sys
import os
import subprocess
import ss
from pprint import PrettyPrinter

pp=PrettyPrinter(indent=4,width=110)
pprint=pp.pprint

def getFilename():
    try:
        filename= sys.argv[1]
    except:
        filename="" 
    while not os.path.isfile(filename):
        filename=input("please enter input file name ")
    return filename



def createList(filename=None,new=False,missing=False):
    #new and missing should never both be true
    head=shipcsv.default_head
    if new:
        PickListData=ss.getAllOrders()
        filename="{}.xlsx".format(toXlsx.output_file)
    elif missing:
        PickListData=data=ss.makeMissing()
        filename="{}{}.xlsx".format(toXlsx.output_file,"-m")
    else:
        PickListData=data=ss.getTodaysOrders()
        filename="{}.xlsx".format(toXlsx.output_file)
    print("Good Morning.You have {} items today.".format(len(PickListData)))
    PickListData=shipcsv.process_Data(PickListData)
    toXlsx.make_list(head,PickListData,filename)
    return filename
#shipcsv.pprint(shipcsv.dept.Memo)

#deprecated
def createMissing(filename=None):
    head=shipcsv.default_head
    pickListData=data=ss.makeMissing()
    print("You have {} items missing.".format(len(pickListData)))
    pickListData=shipcsv.process_Data(pickListData)
    filename="{}{}.xlsx".format(toXlsx.output_file,"-m")
    toXlsx.make_list(head,pickListData,filename)
def askOpenInOffice(filename=None,ext=""):
    sofficePath='\"C:\\Program Files\\LibreOffice 5\\program\\soffice\"'
    otherSofficePath='C:\\Program Files\\LibreOffice 5\\program\\soffice'
   
    #incomplete logic
    if filename==None : 
        filename=toXlsx.output_file
    # print(cmd)
    #cmd="\"\"C:\\Program Files\\LibreOffice 5\\program\\soffice\" --calc \"C:\\Users\Bluesky Shipping\\picklist\\Pick List 07-11-18.xlsx\""
    inp=input("File created would you like to view results in libreoffice calc?(y/n)")
    if inp=='y':
        # print("launching... ",cmd)
        # print()
        # os.system(cmd)
        # subprocess.call(["ls","-l"],shell=True)

        subprocess.call(["echo" ,otherSofficePath,"--calc", filename],shell=True)
        subprocess.Popen([otherSofficePath,"--calc", filename]  )
def askEmail():
    inp=input("Would you like to email pick list out?(y/n)")
    if inp=='y':
        print("sending out")
        #sendOut.main()
def askPrint(filename=toXlsx.output_file_ext):
    inp=input("Would you like to print the pick list? (y/n)")
    if inp=='y':
        print("printing")
        os.startfile(filename,"print")
if __name__=="__main__" :
    try:
        if sys.argv[1]=="-cc":
            dept.clearMemo()
        elif sys.argv[1]=="-l":
            pprint(dept.Memo)
        elif sys.argv[1]== "-o":
            print("-oaccepted")
            print(sys.argv[2])
            createList(filename=sys.argv[2])
            askOpenInOffice(filename=sys.argv[2])
        elif sys.argv[1]=="-m":
            filename=createList(missing=True)
            askOpenInOffice(filename=filename,ext='-m')
            askPrint(filename)
        elif sys.argv[1]=="-r":
            ss.clearSaved()
            createList()
            askOpenInOffice()
        elif sys.argv[1]=="-n":
            createList(None,True)
            askOpenInOffice()
            print("Have a lovely day shipping :)")
    except IndexError:
        filename=createList()
        askOpenInOffice(filename=filename)
        askPrint(filename)
        print("Have a lovely day shipping :)")
