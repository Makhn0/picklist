#/bin/python3
"""module for sending picklist to everyone"""

import smtplib
import getpass
from datetime import datetime
from toXlsx import date_str ,output_file
from email.mime.text import MIMEText 
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders

#TODO 
'''

#alias list='p ${pl}shipcsv.py; p ${pl}toXlsx.py; p ${pl}sendOut.py'
'''
'''basic email info'''
#"""
from_email='erm2718@gmail.com'
password=getpass.getpass("please enter password for {}:".format(from_email))
recipients= [ 'erm2718@gmail.com','eric.maher@blueskysolutions.us','buffalotus@hotmail.com']

#"""
"""

site='@blueskysolutions.us'
from_email='shipping'+site
password=getpass.getpass("please enter password for {}:".format(from_email))
recipients= [a+site for a in [
                'jen.rivero',
                'clinton.early',
                'Corey.Scalese',
                'dennis.narron',
                'elliot.white',
                'erneka.lumpkin',
                'gregory.aycock',
                'jon.bullock',
                'jose.montion',
                'nikki.spaulding'
            ]
    ]+['dan.evans@blueskytradinginc.com']
#PickList_Folder='C://Users/shipping/Desktop'#something like that for windows
#PickList_Folder='~/PickLists/'# for linux
#"""
to_email=", ".join(recipients)



subject='Pick List {}'.format(date_string)
body='Attached : {}'.format(date_string)
attach_fn=output_file

def encode_attachment(attach_filename):
    '''attachment encoding'''
    with open(attach_filename,'rb') as attachment:
        part = MIMEBase('application','octet-stream')
        part.set_payload(attachment.read())
        encoders.encode_base64(part)
        part.add_header('Content-Disposition',"attachment; filename=" +attach_fn)
    return part

def create_MIME(part):
    '''create MIMEMulitpart'''
    msg=MIMEMultipart()
    msg['From']=from_email
    msg['To']=to_email
    msg['Subject']=subject
    msg.attach(  MIMEText(body,'plain')  )
    msg.attach(part)
    return msg


def send(text):
    '''login to server and send email'''
    try:
        server= smtplib.SMTP('smtp.gmail.com',587)
        server.starttls()
        for i in range(0,3):
            try:
                server.login(from_email,password)
            except:
                password=getpass.getpass("login failed try entering password again:")
            else:
                server.sendmail(from_email,recipients,text)
    except:
        print("cannot connect to mail server")
    finally:
        server.quit()
def main():
    part=encode_attachment(attach_fn)
    msg=create_MIME(part)
    text=msg.as_string()
    send(text)

if __name__=="__main__":
    main()
