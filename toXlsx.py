#/bin/python3
"""module for formatting csv data into picklist"""
# assumes we already have the data and it is sorted etc
from openpyxl.workbook import Workbook
from openpyxl.styles import PatternFill,Border,Alignment,Font,Side
import datetime
import shipcsv
import sys

date_format="%m-%d-%Y"

now=datetime.datetime.now()
date_str=now.strftime(date_format)
#PickList_Folder='/c/Users/Bluesky Shipping/picklist/'#for testing
PickList_Folder='C:\\Users\\Bluesky shipping\\Desktop\\Daily Pick Lists'#for testing
output_file="{}\Pick List {}".format(PickList_Folder,date_str)
output_file_ext="{}.xlsx".format(output_file)



wb = Workbook()
ws = wb.active



def simple_fill(color):
    return PatternFill(fill_type='solid',
            start_color=color)

Dept_colors={
        'a':simple_fill('0000FF'),
        'c':simple_fill('FFa500'),
        'l':simple_fill("FF0000"),
        'p':simple_fill("FFFF00"),
        'Ph':simple_fill("FF00FF"),
        'q':simple_fill("00FFFF"),
        'v':simple_fill('00FF00'),
        'z':simple_fill('FFFFFF'),
        "":simple_fill('FFFFFF')
        }
numberFill=PatternFill(fill_type='solid',
                    start_color='C0C0C0'
                        )
AllFont=Font( name='Liberation Sans',
            size=10
           )
QtyFont=Font(name='Liberation Sans',
            size=10,
            bold=True)
QtyAlign=Alignment(horizontal='center',shrink_to_fit=True)
itemAlign=Alignment(shrink_to_fit=True)

def make_number(data):
    for i in range(1,len(data)+1):
            cell=ws["A{}".format(i+1)]
            cell.value="{})".format(i)
            cell.fill=numberFill


def add_data(data):
    for r in range(0,len(data)):
        for c in range(0,3):
            datum=data[r][c]
            cell =ws.cell(column=c+2,row=r+2,value=datum)
            if c==0:
               cell.alignment=QtyAlign
               cell.font=QtyFont
            if c==1:
                pass
        #        cell.alignment=itemAlign
            if c==2:
                cell.fill=Dept_colors[datum]
            
def add_border():
    for row in ws:
        for cell in row:
            cell.border=Border(
                    top=Side(border_style='hair'),
                    bottom=Side(border_style='hair'),
                    right=Side(border_style='hair'),
                    left=Side(border_style='hair')
                        )

def add_font():
    for col in ws.columns:
        if col[0].value != 'QTY' :
            #print('changing {}'.format(col[0].value))
            for cell in col:
                cell.font=AllFont


def add_headings(head):
    for i in range(0,len(head)):
        ws.cell(column=i+1,row=1,value=head[i])
    ws['B1'].alignment=QtyAlign
    ws['B1'].font=QtyFont
        
def space_columns():
    """adjusts the widtch of each colum"""
    # print("spacing colums")
    ws.column_dimensions['A'].width=3#N
    ws.column_dimensions['B'].width=4.5 #QTY
    ws.column_dimensions['C'].width=93#Item
    ws.column_dimensions['D'].width=3 #department
    ws.column_dimensions['E'].width=2 #x
    ws.column_dimensions['F'].width=11#x

def space_rows(N_Rows):
    # print("spacing rows")
    a=ws.row_dimensions
    
    #print("a is ",type(a))
    #print(ws.row_dimensions[1])
    for i in range(0, N_Rows+1):
            #print(ws.row_dimensions[i])
            ws.row_dimensions[i].height=12
    print()
    #for row in ws.row_dimensions.items():

        #print("row is ",type(row))
        #print(row)
        #row.height=100
def make_list(header,data,filename=None):
    """edit file"""
    ws.page_setup.orientation=ws.ORIENTATION_LANDSCAPE
    add_headings(header)
    make_number(data)
    add_data(data)
    add_border()
    add_font()
    space_columns()
    space_rows(len(data))
    """save file"""
    if filename==None:
        wb.save(output_file_ext)
    else:
        wb.save(filename)
if __name__=="__main__":
    #shipcsv
    make_list(shipcsv.default_head,[[1,"ffooo","v"]])
    print()
    print("height",ws.row_dimensions[1].height)
