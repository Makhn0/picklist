import xlwt
import csv
import datetime


""" to be program that converts csv downloaded from shipstation to formatted .xls"""

'''
sheet.write(r,1,label="{}".format(i[1][1]))
    #border on all items
    #landscape
    #.xls
    #n) col A
    #bold, centered colB
    #long width colC
    #small w/ colors colD
    #small x colE
    #big x colF
    '''


    '''#create Heading'''
Headings=["","QTY","Name","","x","x"]

    rowsRaw=[
        (1,"power thing"),
        (2,"phone"),
        (3,"gpu"),
        (1,"battery")
        ]

allborders= xlwt.Borders()
allborders.left=1
allborders.right=1
allborders.top=1
allborders.bottom=1



'''create styles '''
#libited color codes can ve found at
#https://github.com/python-excel/xlwt/blob/109a052439c2d346c658595a1e664e07f6a9f4df/xlwt/Style.py
# search for  _colour_map_text
styleA = xlwt.XFStyle()
styleA.pattern.pattern=2
styleA.pattern.pattern_back_colour =0x30

styleC = xlwt.XFStyle()
styleC.pattern.pattern=2
styleC.pattern.pattern_back_colour =0x2B

styleL = xlwt.XFStyle()
styleL.pattern.pattern=2
styleL.pattern.pattern_back_colour =0x0A

styleV = xlwt.XFStyle()
styleV.pattern.pattern=2
styleV.pattern.pattern_back_colour =0x0B
#patternV.pattern_back_color=0x00ff00




colStyle={}
for i,name in enumerate(Headings):

    print(i,name)
    colStyle[i]=xlwt.XFStyle()
    colStyle[i].borders=allborders


'''#colStyle[2].pattern.pattern=xlwt.Pattern.SOLID_PATTERN'''
#pattern=xlwt.Pattern()
colStyle[0].pattern.pattern=2#xlwt.Pattern.SOLID_PATTERN
colStyle[0].pattern.pattern_back_colour=0x17


'''bold and centered'''
fnt=xlwt.Font()
fnt.bold=True
align=xlwt.Alignment()
align.alignment=3
#fnt.horizontal=2


colStyle[1].font=fnt
colStyle[1].alignment=align


dept={
    "[a-zA-Z]{1,2}-\d\w*$":('a',styleA),
    "([Ll]aptop | [mM]obile)":('l',styleL),
    "":('c',styleC),
    "{.*}\w*$" :('v',styleV)
     }

'''#look up sorting'''
i=0
j=0
def getDep(inp):
global i,j
for exp,value in dept.items():
        if i==j:
            if j>= len(dept):
                j=0
                else:
                    j+=1
                    i=0
                    print (value)
                    return  value

        i+=1
#return ("p",styleA)
#if inp.matches(exp): return value
return ('a',styleA)


'''assigning deps'''
rows=[  [ qty,name,getDep(name) ] for qty,name in rowsRaw ]


''' 
sort rows
##TODO
#consolidate repeat items?
'''
def consolidate(rows):
    for i in range(0,len(rows)):
        for j in rows[i:]:
            if rows[i][1]==j[1]:
                rows[i][0]+=j[0]
                del(j)
            else: break


doc =xlwt.Workbook(encoding='ascii',style_compression=0)
sheet= doc.add_sheet("sheet 1")

'''headings'''
for i in range(0,len(Headings)):
    sheet.write(0,i,label=Headings[i],
        style=colStyle[i]
    )

'''data'''
for r,row in enumerate(rows):
        '''  # create numbers'''
    r=r+1
    qty,name,dept=row
    sheet.write(r,0,label="{})  ".format(r),
        style=colStyle[0]
        )

    sheet.write(r,1,label="{qty}".format(qty=qty),
        style=colStyle[1]
    )
    sheet.write(r,2,label="{name}".format(name=name),
        style=colStyle[2]
    )

    print(dept)
    print(dept[0],dept[1].pattern.pattern_back_colour)
    sheet.write(r,3,label="{dept}".format(dept=dept[0]),
            style=dept[1]
        )

    sheet.write(r,4,#label=,"{dept}".format(dept=i[1][2]),
            style=colStyle[3]
        )
    sheet.write(r,5,#label=,"{dept}".format(dept=i[1][2]),
            style=colStyle[4]
        )


"""size columns"""
sheet.col(0).width=1000
sheet.col(1).width=1400
sheet.col(2).width=20000
sheet.col(3).width=800
sheet.col(4).width=800
sheet.col(5).width=3000

"""save files"""
datestr=datetime.date.today().strftime("%m-%d-%y")

#doc.save("picklist {}.xls".format(datestr))
doc.save("document")


