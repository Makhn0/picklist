import json
import re
import sys

# sys.stdout.errors='replace'
"""#define functions for department lookup"""

MemoFileName="Memo"
def readMemo():
    MemoFileRead=open(MemoFileName)
    MemoStr=MemoFileRead.read()
    Memo=json.loads(MemoStr)
    MemoFileRead.close()
    return Memo

Memo=readMemo()


deptRE={"a" :"|".join([
                "[a-os-zA-OS-Z]{1,2}-\d\w*$",
                "MS[\s-]?\d{1,3}",
               "GP\d{1,3}"]),
        "c" :"|".join([
                    "AMD",
                   "CPU"]),
        "l" :"|".join([
                    "[Ll]aptop",
                    "[Mm]obile"]),
        "p" :"|".join(["pc"]),
        #"Ph":"|".join(["P[AS]-\d{1,3}",
        #            "[Pp]hone"]),
        "q":"|".join(["QP[\s-][123]",
                    "{QP}",
                    "Chili Technology"]),
        "v" :"|".join(["{.*}\w*$"])
    }


def writeMemo():
    with open(MemoFileName,"w") as f:
        f.write(json.dumps(Memo,indent=4))
def clearMemo():
    with open(MemoFileName,"w")as f:
        f.write(json.dumps({},indent=4))
def match(name,exp):
    """returns true if name matches exp"""
    m=re.search(exp,name)
    #print("Matching",name,exp, m)
    return m!=None
    
def getMemo(name):
    #pprint(Memo)
    #print("Finding \033[95m\"{}\"\x1b[0m...".format(name))
    try:
        return Memo[name]
    except KeyError:
       return None

def setMemo(name):
    """asks user for input and sets the department name for the item"""
    # valid=False

    inp="NOTINDEPT"
    while not( inp in deptRE.keys() or inp=="") :
        try:
            inp=input("Enter department name for \033[95m\"{}\"\x1b[0m:".format(name))
        except UnicodeEncodeError as e :
            print(e)
            print("try calling Export PYTHONIOENCODING=utf-8")

     #   valid= inp in deptRE.keys() or inp==""

    if inp =="": 
        skip=input("Would you like to set the dept to z so it appears at the bottom of the spreadsheet and edit it later?(y/n)")
        if skip=='y':
            print("Setting dept to z in spreadsheet?")
            inp='z'
        else:
            print("Leaving {} unassigned".format(name))
            inp=""
    #pprint(Memo)
    Memo[name]=inp 
    return inp

def confirmDep(name,dep):
    answer=input("Please confirm that \033[95m\"{}\"\x1b[0m:, is in department \'{}\'  (y/n)".format(name,dep))
    if answer == 'y':
        return True
    return False

skuByDep={"a" :["ms"],
        "c" :[],
        "l" :[""],
        "p" :["pc"],
        #"Ph":[""],
        "q":["{QP}"],
                    
        "v" :["ms"]
    }


def getDepBySku(sku):
    if sku == None: return None
    for dep,skus in skuByDep.items():
        if sku in skus: return dep
    return None
    
def getDep(name,sku=None):

    """returns the department of the item with name or sku"""
    """First looks at sku of the item if entered"""
    depBySku=getDepBySku(sku)
   
    """then trys to look up in Memo"""
    a=getMemo(name)
    if a: return a
    """if that fails it trys to match regexes"""
    '''
    for dep,exp in deptRE.items():
        if match(name,exp):
            #confirm with user that the regex mactch was right
            if(confirmDep(name,dep)):
                Memo[name]=dep
                print(name,dep)
                return dep
                '''
    # commented confirming regex predicting
    # regexp predicting just ruined the flow of entering in departments by hand
    """if that fails it asks the user for the name"""
    return setMemo(name)

def addDeps(data):
    """adds the department column onto the database"""
    for row in data:
        # print('row', row[1][0:11])
        
        try:
            #TODO take out if for sku change
            if not row[2]:
                row[2]=getDep(row[1])
        except IndexError:
            #print('not row[2]')
            row.append(getDep(row[1]))
    return data
def add_saveDeps(data):
    """adds the department column into data array and saves new name,dept pairs to Memo file"""
    data= addDeps(data)
    writeMemo()
    return data


