#/bin/python3
"""module for sorting, and adding department names """
#TODO Rename module
import csv
import sys
import dept
import ss
from pprint import PrettyPrinter 

pp=PrettyPrinter(indent=4,width=110)
pprint=pp.pprint

#import toXlsx


"""rest of the data"""
default_head=['','QTY','Name','','x','x']


def get_HeadData(fn):
    """returns tuple (head, data) getting Data from file"""
    data=[]
    #print("reading data from {}".forma(fn))
    with open(fn) as f:
       datad=csv.reader(f,delimiter=',') 
       for row in datad:
            data.append(row)
    head=data[0]
    data=data[1:]
    """convert to int"""
    #can get rid of if we never use combine
    for i in data:
        i[0]=int(i[0])
        i[1]=i[1].strip()
    return head,data


def write_HeadData(fn,head,data):
    """writing to file"""
    print("Writing to {}".format(fn))
    with open(fn,"w") as f:
            f.write(','.join(head))
            f.write('\n')
            for i in data:
                f.write(",".join([str(x) for x in i]))
                f.write('\n')

def make_HeadData(fn): 
    """Prints the input and output for debug purposes only"""
    head,data=get_HeadData(fn)
    process_Data(data)
    return default_head,data
    
def combine(data):
    """Combines repeat orders into a single item with quantity equal to the sum of the quanities"""
    last=[0,None,None]
    for i, row in enumerate(data):
        print(i,'current ',row,' last ',last)
        if  row[1]==last[1] and row[2]==last[2]:
            print('Removing',row)
            last[0]+=row[0]
            data[i]=0
        else: last = row
    return [row for row in data if not row==0]

def sort_Data(data):
    """Sorts by department, then by name , then by QTY"""
    sortKey= lambda item: (item[2].title(),item[1], -int(item[0]))
    data.sort(key=sortKey)
    return data

def sort_DataSku(data):
    """Sorts by Sku, then name, then by Qty"""
    """same as sort_data, but could change later"""
    sortKey=lambda item: (item[2].upcase(), item[1],-int(item[0]))
    data.sort(key=sortKey)
    return data
def process_Data(data,Combine=False):
    """Processes the data for writing to file or std"""
    data= dept.add_saveDeps(data)
    data= sort_Data(data) 
    if Combine==True:
        data=combine(data)
    return data 



if __name__=="__main__":
    pass
